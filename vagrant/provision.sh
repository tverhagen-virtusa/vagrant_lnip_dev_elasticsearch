#!/usr/bin/env bash

# update apt
sudo apt update

# install java
sudo apt install -y openjdk-8-jre-headless

# install elasticsearch
#wget https://download.elasticsearch.org/elasticsearch/elasticsearch/elasticsearch-1.1.1.deb
wget https://artifacts.elastic.co/downloads/elasticsearch/elasticsearch-6.5.4.deb
sudo dpkg -i elasticsearch-6.5.4.deb
sudo service elasticsearch start

# Let ElasticSearch not only accept localhost network traffic
# https://www.elastic.co/guide/en/elasticsearch/reference/current/modules-network.html
sudo sed -i '/#network.host: 192.168.0.1/a network.host: _site_' /etc/elasticsearch/elasticsearch.yml

# install head
# sudo /usr/share/elasticsearch/bin/plugin -install mobz/elasticsearch-head
